(function ($) {

// @todo Array syntax required; 'break' is a predefined token in JavaScript.
Drupal.wysiwyg.plugins['vine_plugins'] = {
  
  /**
   * Execute the button.
   */
  invoke: function(data, settings, instanceId) {
    if (data.format == 'html') {
      // Prevent duplicating a teaser break.
      if ($(data.node).is('img.wysiwyg-break')) {
        return;
      }
      var content = prompt('vine ID');
	  var vine = '<div class="inserted_vine"><iframe src="https://vine.co/v/'+content+'/embed/simple" width="600" height="600" frameborder="0"></iframe></div>';
    }
    else {
      // Prevent duplicating a teaser break.
      // @todo data.content is the selection only; needs access to complete content.
      var content = prompt('vine ID');
	  var vine = '<div class="inserted_vine"><iframe src="https://vine.co/v/'+content+'/embed/simple" width="600" height="600" frameborder="0"></iframe></div>';
    }
    if (typeof content != 'undefined') {
      Drupal.wysiwyg.instances[instanceId].insert(vine);
    }
  },

  /**
   * Replace all <!--break--> tags with images.
   */
  attach: function(content, settings, instanceId) {
    //content = content.replace(/<!--test-->/g, this._getPlaceholder(settings));
    return content;
  },

  /**
   * Replace images with <!--break--> tags in content upon detaching editor.
   */
  detach: function(content, settings, instanceId) {
    var $content = $('<div>' + content + '</div>'); // No .outerHTML() in jQuery :(
    // #404532: document.createComment() required or IE will strip the comment.
    // #474908: IE 8 breaks when using jQuery methods to replace the elements.
    // @todo Add a generic implementation for all Drupal plugins for this.
    return $content.html();
  },

  /**
   * Helper function to return a HTML placeholder.
   
  _getPlaceholder: function (settings) {
    return '<img src="' + settings.path + '/images/spacer.gif" alt="&lt;--test-&gt;" title="&lt;--break--&gt;" class="wysiwyg-break drupal-content" />';
  }*/
};

})(jQuery);
