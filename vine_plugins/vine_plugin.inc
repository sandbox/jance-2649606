 <?php
 /* Implements WYSIWYG's hook_INCLUDE_plugin().
 */
function vine_wysiwyg_vine_plugin_plugin() {
  $plugins['vine_plugins'] = array(
    'title' => t('Vine Insert'),
    'icon path' => drupal_get_path('module', 'vine_wysiwyg') . '/vine_plugins/images',
    'icon file' => 'icon.gif',
    'icon title' => t('Add Vine'),
    'js path' => drupal_get_path('module', 'vine_wysiwyg') . '/vine_plugins',
    'js file' => 'editor_plugin.js',
    'extended_valid_elements' =>
            array('iframe[src|width|height|frameborder|scrolling]'),
    'settings' => array(),
  );
  
  //dpm($plugins);

  return $plugins;
}
